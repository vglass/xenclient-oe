require recipes-graphics/xorg-driver/xorg-driver-video.inc
LIC_FILES_CHKSUM = "file://COPYING;md5=d8cbd99fff773f92e844948f74ef0df8"

DESCRIPTION = "X.Org X server -- OpenXT framebuffer display driver"
PR = "${INC_PR}.1"
DEPENDS = "xserver-xorg kernel-module-openxtfb"
#Always grab the latest from git.
SRC_URI = " \
  git://gitlab.com/vglass/xf86-video-openxtfb.git;protocol=ssh;branch=${OPENXT_BRANCH} \
  file://10-openxtfb.rules \
  file://display_hotplug.sh \
"
SRCREV = "${AUTOREV}"
PV = "0+git${SRCPV}"
S = "${WORKDIR}/git"
FILES_${PN} += "  \
  ${sysconfdir}/udev/rules.d/10-openxtfb.rules \
  /usr/share/openxt/display_hotplug.sh \
"
#
# In addition to our main driver, also install our hotplug scripts and rules.
#
do_install_append() {
  #Install our udev rule...
  install -d ${D}${sysconfdir}/udev/rules.d/
  install -m 644 ${WORKDIR}/10-openxtfb.rules ${D}${sysconfdir}/udev/rules.d/10-openxtfb.rules
  #... and the script it calls.
  install -d ${D}/usr/share/openxt
  install -m 755 ${WORKDIR}/display_hotplug.sh ${D}/usr/share/openxt/display_hotplug.sh
}
