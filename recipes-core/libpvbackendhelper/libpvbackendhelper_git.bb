SUMMARY = "Userspace backend for vglass"
SECTION = "libs"

LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://LICENSE;md5=fc178bcd425090939a8b634d1d6a9594"

DEPENDS = "xen libivc2"
RDEPENDS_${PN} = "libivc2"
#OXT: Removed libivc from DEPENDS and RDEPENDS, should be replaced by libivc2
PR = "r0"

SRCREV = "${AUTOREV}"
PV = "0+git${SRCPV}"

S = "${WORKDIR}/git"

SRC_URI = "git://gitlab.com/vglass/pv-display-helper.git;protocol=ssh;branch=${OPENXT_BRANCH}"

#Specify the components that should make it into each of the relevant files.

FILES_${PN} += "/usr/lib/libpvbackendhelper.so"

FILES_${PN}-dev += "/usr/lib/pv_display_backend_helper.h"
#FILES_${PN}-dev += "/usr/lib/pv_driver_interface.h"
#FILES_${PN}-dev += "/usr/lib/data-structs/list.h"
FILES_${PN}-dev += "/usr/lib/common.h"

FILES_${PN}-dbg += "/usr/lib/.debug/*"

FILES_SOLIBSDEV = ""

do_compile() {
    oe_runmake backend
}

do_install() {
    install -D ${S}/libpvbackendhelper.so ${D}/usr/lib/libpvbackendhelper.so
    install -D ${S}/pv_display_backend_helper.h ${D}/usr/include/pv_display_backend_helper.h
}
