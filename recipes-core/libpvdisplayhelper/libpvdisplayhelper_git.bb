SUMMARY = "Userspace equivalent to the pv display helper kernel module"
HOMEPAGE = "http://www.ainfosec.com"
SECTION = "libs"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=fc178bcd425090939a8b634d1d6a9594"
DEPENDS = "libivc2 xen"
RDEPENDS_${PN} = "libivc2"
PV = "git${SRCPV}"
SRCREV = "${AUTOREV}"
S = "${WORKDIR}/git"
SRC_URI = "git://gitlab.com/vglass/pv-display-helper.git;protocol=ssh;branch=${OPENXT_BRANCH}"
#Specify the components that should make it into each of the relevant files.
FILES_${PN} += "/usr/lib/libpvdisplayhelper.so"
FILES_${PN}-dev += "/usr/lib/*.h"
FILES_${PN}-dbg += "/usr/lib/.debug/*"
FILES_SOLIBSDEV = ""
do_compile() {
    oe_runmake user
}
do_install() {
    install -D ${S}/libpvdisplayhelper.so ${D}/usr/lib/libpvdisplayhelper.so
    install -D ${S}/pv_display_helper.h ${D}/usr/include/libpvdisplayhelper.h
    install -D ${S}/pv_driver_interface.h ${D}/usr/include/pv_driver_interface.h
    install -D ${S}/common.h ${D}/usr/include/common.h
    install -d ${D}/usr/include/data-structs
    install -D ${S}/data-structs/list.h ${D}/usr/include/data-structs/list.h
    install -D ${S}/data-structs/hash.h ${D}/usr/include/data-structs/hash.h
    install -D ${S}/data-structs/hashtable.h ${D}/usr/include/data-structs/hashtable.h
}
PR = "r1"
