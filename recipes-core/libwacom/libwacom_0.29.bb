SUMMARY = "Library to handle libraries, configuration, and diagnostic tools for Wacom tablets running under Linux."
HOMEPAGE = "https://sourceforge.net/projects/linuxwacom"
SECTION = "libs"
DEPENDS = "libgudev glib-2.0"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://COPYING;md5=40a21fffb367c82f39fd91a3b137c36e"
PR = "r2"
SRC_URI = " \
  https://sourceforge.net/projects/linuxwacom/files/${BPN}/${BP}.tar.bz2 \
"
SRC_URI[md5sum] = "3a6b614b57518b25a102dcd9e51d6681"
SRC_URI[sha256sum] = "9cea00e68ddf342c3f749f6628d33fd3646f1937d7c57053ec6c5728d9a333b6"
inherit autotools pkgconfig
