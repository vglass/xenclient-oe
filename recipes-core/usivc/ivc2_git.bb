SUMMARY = "libivc specific for dom0"
DESCRIPTION = "This recipe builds/provides libivc for dom0."
LIC_FILES_CHKSUM = "file://../../../LICENSE;md5=1676875b46a1978a82e9a6816db4e8a5"
LICENSE = "BSD"

DEPENDS += "qtbase libxenbe"

SRC_URI = "git://gitlab.com/vglass/ivc.git;protocol=ssh;branch=${OPENXT_BRANCH} \
    file://ivcdaemon.initscript \
"

SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git/src/usivc/ivcdaemon"

inherit qmake5

FILES_${PN} += "${bindir}/ivcdaemon"
FILES_${PN}-dev += "${libdir}/*.h"
FILES_${PN}-dbg += "${libdir}/.debug/*"
FILES_SOLIBSDEV = "" 

do_install () {
    #Finally, copy our workaround initscript to /etc/init.d.
    install -d "${D}/etc/init.d"
    install -m 755 "${WORKDIR}/ivcdaemon.initscript" "${D}/etc/init.d/ivcdaemon"
    
    install -d "${D}/${bindir}"
    install -m 755 "${WORKDIR}/bin/ivcdaemon" "${D}/${bindir}/ivcdaemon"
}
