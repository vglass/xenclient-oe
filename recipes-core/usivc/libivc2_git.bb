SUMMARY = "libivc specific for dom0"
DESCRIPTION = "This recipe builds/provides libivc for dom0."
LIC_FILES_CHKSUM = "file://../../../LICENSE;md5=1676875b46a1978a82e9a6816db4e8a5"
LICENSE = "BSD"

DEPENDS += "qtbase libxenbe"

SRC_URI = "git://gitlab.com/vglass/ivc.git;protocol=ssh;branch=${OPENXT_BRANCH} \
           file://add-ringbuffer.patch \
"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git/src/usivc/ivclib"

inherit qmake5

FILES_${PN} += "${libdir}/libivc.so"
FILES_${PN}-dev += "${libdir}/*.h"
FILES_${PN}-dbg += "${libdir}/.debug/*"
FILES_SOLIBSDEV = "" 

#install the pieces manually.
do_install(){
    #... install the main library...
    install -D ${B}/..${base_libdir}/libivc.so -t ${D}${libdir}/
    #... and install its headers.
    for headerfile in libivc libivc_types libivc_debug; do
        install -D ${WORKDIR}/git/include/core/${headerfile}.h ${D}/${includedir}/${headerfile}.h
    done
}
