# Copyright (C) 2015 Assured Information Security, Inc.
# Recipe released under the MIT license (see COPYING.MIT for the terms)

SUMMARY = "Userspace library for accessing the InterVM Communication modules"
DESCRIPTION = " \
    This library provides a set of userspace functions for interacting with \
    the IVC (InterVM Communication) kernel modules. These functions can be used \
    to gain simple file and shared-memory communications channel between VMs \
    running on the Xen hypervisor. \
"
AUTHOR = "\
    Kyle J. Temkin <temkink@ainfosec.com> \
    Brendan Kerrigan <kerriganb@ainfosec.com> \
"
#License information for the recipe.
LIC_FILES_CHKSUM = "file://../../LICENSE;md5=1676875b46a1978a82e9a6816db4e8a5"
LICENSE = "BSD"

#Depend on the IVC kernel module for runtime use.
#TODO: Replace this with a virtual package, provided by kernel-module-ivc, 
#to match OE specs.
RDEPENDS_${PN} += "kernel-module-ivc"

#Pull this down from AIS' gitlab, for now. Ideally, this will be on GitHub.
SRC_URI = "git://gitlab.com/vglass/ivc.git;protocol=ssh;branch=${OPENXT_BRANCH}"

#Always use the latest version from our GIT source.
PV     = "0+git${SRCPV}"
SRCREV = "${AUTOREV}"

#Build only the userspace section of the relevant repository.
S = "${WORKDIR}/git/src/us"

#Specify the components that should make it into each of the relevant files.
FILES_${PN} += "${libdir}/libivc.so"
FILES_${PN}-dev += "${libdir}/*.h"
FILES_${PN}-dbg += "${libdir}/.debug/*"
FILES_SOLIBSDEV = "" 

#Use the cmake build system.
inherit cmake

#Our CMake recipe currently doesn't install headers, so we'll
#install the pieces manually.
do_install(){

    #... install the main library...
    install -D ${B}/${base_libdir}/libivc.so -t ${D}/${libdir}/

    #... and install its headers.
    for headerfile in libivc libivc_types libivc_debug; do
        install -D ${WORKDIR}/git/include/core/${headerfile}.h ${D}/${includedir}/${headerfile}.h
    done
}
