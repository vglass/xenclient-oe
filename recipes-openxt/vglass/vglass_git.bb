DESCRIPTION = "Graphics Stack"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://LICENSE;md5=f1e379c1d5588582790922b24502f0b8"
#OXT: Replaced libivc with libivc2....and kernel-module-ivc should be replaced ?
DEPENDS = "dbus libdrm qtbase libinput libivc2 libpvdisplayhelper libpvbackendhelper libxenbackend"

RDEPENDS_${PN} += "dbus qtbase libinput ivc2 libivc2 libpvdisplayhelper libpvbackendhelper xenclient-keyboard-list bash"

SRCREV = "${AUTOREV}"
PV = "0+git${SRCPV}"

inherit qmake5
inherit update-rc.d

INITSCRIPT_NAME = "vglass"
INITSCRIPT_PARAMS = "defaults 98 0"

S = "${WORKDIR}/git"

# Should get rid of this when we fix the source includes (change #include "drm.h" to #include "drm/drm.h")
export STAGING_DIR_TARGET
export EXTRA_INCLUDE_PATH = "${WORKDIR}/build"
export SVLIB_PRODUCTION

SRC_URI = "git://gitlab.com/vglass/glass.git;protocol=ssh;branch=${OPENXT_BRANCH} \
           file://discrete_gfx.conf \
           "

FILES_${PN} += "${bindir}/glass"
FILES_${PN} += "${libdir}/*.so"
FILES_${PN} += "/etc/mosaic/*.png"
FILES_${PN} += "/etc/mosaic/inputserver_config"
FILES_${PN}-dev += "${includedir}/common/include/*.h"
FILES_${PN}-dev += "${includedir}/toolstack/include/*.h"
FILES_${PN}-dev += "${includedir}/toolstack/xenmgr/*.h"
FILES_${PN}-dev += "${includedir}/common/include/gsl/*"
FILES_${PN}-dbg += "${libdir}/.debug/*"
FILES_SOLIBSDEV = ""

do_install() {
    install -d ${D}${includedir}
    install -d ${D}${includedir}/common/include
    install -d ${D}${includedir}/common/include/gsl
    install -d ${D}${includedir}/toolstack/include
    install -d ${D}${includedir}/toolstack/xenmgr

    install -m 755 ${S}/common/include/*.h ${D}${includedir}/common/include
    install -m 755 ${S}/toolstack/include/*.h ${D}${includedir}/toolstack/include
    install -m 755 ${S}/toolstack/xenmgr/*.h ${D}${includedir}/toolstack/xenmgr
    install -m 755 ${S}/common/include/gsl/* ${D}${includedir}/common/include/gsl

    install -d ${D}${libdir}
    install -m 755 ${S}/lib/*.so ${D}${libdir}

    install -d ${D}${bindir}
    
    install -m 755 ${S}/bin/glass ${D}${bindir}
    install -m 755 ${S}/bin/vglass ${D}${bindir}

    install -d ${D}/etc/mosaic
    install -m 755 ${S}/configs/inputserver_config ${D}/etc/mosaic
    install -m 755 ${S}/*.png ${D}/etc/mosaic

    #Include the modprobe rules that prevent loading of discrete graphics modules
    #from outside of the display handler.
    install -d ${D}/etc/modprobe.d
    install -m 755 ${WORKDIR}/discrete_gfx.conf ${D}/etc/modprobe.d/discrete_gfx.conf

    install -d ${D}/etc/init.d
    install -m 755 ${S}/init.d/vglass ${D}/etc/init.d

    install -d ${D}/usr/share/vglass
    install -m 755 ${S}/bin/discrete_gfx_modprobe.sh ${D}/usr/share/vglass
}
