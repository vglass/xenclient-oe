DESCRIPTION = "Linux kernel for OpenXT service VMs."

include linux-appends.inc

# Use the one from meta-openembedded/meta-oe
require recipes-kernel/linux/linux.inc
require recipes-kernel/linux/linux-openxt.inc

PV_MAJOR = "${@"${PV}".split('.', 3)[0]}"

FILESEXTRAPATHS_prepend := "${THISDIR}/patches:${THISDIR}/defconfigs:"
SRC_URI += "${KERNELORG_MIRROR}/linux/kernel/v${PV_MAJOR}.x/linux-${PV}.tar.xz;name=kernel \
    file://bridge-carrier-follow-prio0.patch \
    file://privcmd-mmapnocache-ioctl.patch \
    file://xenkbd-tablet-resolution.patch \
    file://acpi-video-delay-init.patch \
    file://skb-forward-copy-bridge-param.patch \
    file://dont-suspend-xen-serial-port.patch \
    file://extra-mt-input-devices.patch \
    file://tpm-log-didvid.patch \
    file://blktap2.patch \
    file://export-for-xenfb2.patch \
    file://allow-service-vms.patch \
    file://intel-amt-support.patch \
    file://disable-csum-xennet.patch \
    file://pci-pt-move-unaligned-resources.patch \
    file://pci-pt-flr.patch \
    file://realmem-mmap.patch \
    file://netback-skip-frontend-wait-during-shutdown.patch \
    file://xenbus-move-otherend-watches-on-relocate.patch \
    file://netfront-support-backend-relocate.patch \
    file://konrad-ioperm.patch \
    file://fbcon-do-not-drag-detect-primary-option.patch \
    file://usbback-base.patch \
    file://hvc-kgdb-fix.patch \
    file://pciback-restrictive-attr.patch \
    file://thorough-reset-interface-to-pciback-s-sysfs.patch \
    file://tpm-tis-force-ioremap.patch \
    file://netback-vwif-support.patch \
    file://gem-foreign.patch \
    file://alps-touchpad-quirk.patch \
    file://xen-txt-add-xen-txt-eventlog-module.patch \
    file://xenpv-no-tty0-as-default-console.patch \
    file://xsa-155-qsb-023-add-RING_COPY_RESPONSE.patch \
    file://xsa-155-qsb-023-xen-blkfront-make-local-copy-of-response-before-usin.patch \
    file://xsa-155-qsb-023-xen-blkfront-prepare-request-locally-only-then-put-i.patch \
    file://xsa-155-qsb-023-xen-netfront-add-range-check-for-Tx-response-id.patch \
    file://xsa-155-qsb-023-xen-netfront-copy-response-out-of-shared-buffer-befo.patch \
    file://xsa-155-qsb-023-xen-netfront-do-not-use-data-already-exposed-to-back.patch \
    file://defconfig \
    "

LIC_FILES_CHKSUM = "file://COPYING;md5=bbea815ee2795b2f4230826c0c6b8814"
SRC_URI[kernel.md5sum] = "bf96b6783a2d11178a2aaa3cf376f975"
SRC_URI[kernel.sha256sum] = "293ec1ae0f6b3b4be83a217224b51d137f2163cf2d9d294eecf5d0a354e4e29d"

do_configure_append_openxt-installer(){
    #Enable kernel list debugging, which reportedly
    #also works around several kernel bugs, to the point
    #that several upstream distributions enable it.
    enable_kernel_option DEBUG_LIST
    
    #Rework the current the kernel configuration to include the
    #Nouveau and Radeon DRM drivers.
    enable_kernel_module DRM_RADEON
    enable_kernel_module DRM_NOUVEAU
    
    #Enable the VMWare graphics driver by default
    enable_kernel_module DRM_VMWGFX
    
    #Enable a collection of touchscreen/pen drivers that will be
    #used by the displayhandler.
    enable_kernel_module HID_WACOM
    enable_kernel_module I2C_HID
    
    #Enable I2C drivers that support our touchscreen devices.
    enable_kernel_module I2C_DESIGNWARE
    enable_kernel_module I2C_DESIGNWARE_PLATFORM
    
    #Enable modules for touchscreen support on HP Elite x2
    enable_kernel_module PINCTRL_SUNRISEPOINT
    enable_kernel_module MFD_INTEL_LPSS_PCI
}

do_configure_append_xenclient-dom0(){
    #Enable kernel list debugging, which reportedly
    #also works around several kernel bugs, to the point
    #that several upstream distributions enable it.
    enable_kernel_option DEBUG_LIST
    #Rework the current the kernel configuration to include the
    #Nouveau and Radeon DRM drivers.
    enable_kernel_module DRM_RADEON
    enable_kernel_module DRM_NOUVEAU
    #Enable the VMWare graphics driver by default
    enable_kernel_module DRM_VMWGFX
    #Enable a collection of touchscreen/pen drivers that will be
    #used by the displayhandler.
    enable_kernel_module HID_WACOM
    enable_kernel_module I2C_HID
    #Enable I2C drivers that support our touchscreen devices.
    enable_kernel_module I2C_DESIGNWARE
    enable_kernel_module I2C_DESIGNWARE_PLATFORM
    #Enable modules for touchscreen support on HP Elite x2
    enable_kernel_module PINCTRL_SUNRISEPOINT
    enable_kernel_module MFD_INTEL_LPSS_PCI
}

do_configure_append_xenclient-uivm(){
    enable_kernel_option FB
    enable_kernel_option FB_VIRTUAL
    enable_kernel_option FIRMWARE_EDID
    enable_kernel_option FB_CMDLINE
    enable_kernel_option FB_NOTIFY
    enable_kernel_option FB_CFB_FILLRECT
    enable_kernel_option FB_CFB_COPYAREA
    enable_kernel_option FB_CFG_IMAGEBLIT
    enable_kernel_option FB_BACKLIGHT
    enable_kernel_option FB_SYS_FILLRECT
    enable_kernel_option FB_SYS_COPYAREA
    enable_kernel_option FB_SYS_IMAGEBLIT
    enable_kernel_option FB_SYS_FOPS
    enable_kernel_option FB_DEFERRED_IO
    enable_kernel_option FB_MODE_HELPERS
    enable_kernel_option XEN_FBDEV_FRONTEND
    #Enable kernel list debugging, which reportedly
    #also works around several kernel bugs, to the point
    #that several upstream distributions enable it.
    enable_kernel_option DEBUG_LIST
}
