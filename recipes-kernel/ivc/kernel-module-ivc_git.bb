SUMMARY = "Linux kernel driver for IVC InterVM communications"
DESCRIPTION = " \
    This loadable kernel module provides the backend support for \
    inter-domain communications using the IVC InterVM Communications \
    framework.\
    \
    This module provides an interface for userspace processes to \
    communicate securely across VM boundaries. It is recommended \
    that libivc be used as a frontend for using this module. \
"
LICENSE  = "BSD"
LIC_FILES_CHKSUM = "file://LICENSE;md5=1676875b46a1978a82e9a6816db4e8a5"
SECTION = "kernel/modules"

PR = "18"

#Mark this as one of the potential providers for the IVC driver.
#This is mostly to clue in that this isn't the only possible provider
#for the IVC backend-- but rather the intended provider for linux platforms.
RDEPENDS_${PN}  = "kernel"
DEPENDS         = "virtual/kernel"

#The name of the module to be produced.
KERNEL_MODULE = "ivc"

SRCREV = "${AUTOREV}"
PV = "0+git${SRCPV}"

SRC_URI = " \
    git://gitlab.com/vglass/ivc.git;protocol=ssh;branch=${OPENXT_BRANCH} \
    file://compiler.patch \
"

S = "${WORKDIR}/git"
KERNEL_MODULE_BUILD_DIR = "${S}/src"
MODULES_INSTALL_TARGET="install_kernel"
KERNEL_MODULE_PACKAGE_SUFFIX = ""

#Provide our build system with information regarding the build.
export IVC_BASE_DIR = "${S}"
inherit oxt-module

FILES_${PN} += "/lib/modules"

#
# We'll use make directly to create the module-- it will, in turn, call
# Kbuild.
#
do_compile () {
    oe_runmake kernel 
}

#
# For now, we'll install the kernel module manually, rather than using the kernel
# modules_install. Later, it may be desireable to get this down to something that
# can be done just by calling "modules_install".
#
do_install () {
    #... and copy each of IVC's core headers to its source path.
    install -d "${D}${KERNEL_MODULE_INCLUDE_DIR}"
    install -m 0644 ${S}/include/core/libivc*h "${D}${KERNEL_MODULE_INCLUDE_DIR}"

    install -d ${D}/lib/modules/${KERNEL_VERSION}/extra
    install -m 0755 ${S}/src/ivc.ko ${D}/lib/modules/${KERNEL_VERSION}/extra/ivc.ko

#Finally, copy our workaround initscript to /etc/init.d.
#install -d "${D}/etc/init.d"
#install -m 755 "${WORKDIR}/ivc.initscript" "${D}/etc/init.d/ivc"
}
